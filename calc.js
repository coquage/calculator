export function calc(a,b,sign){
    switch(sign){
        case '+':
            return a+b;            
        case '-':
            return a-b;           
        case '*':
            return a*b;            
        case '/':
            return b==0? 'на ноль делить нельзя!': a/b;                        
        default:
            return'некорректный оператор, пожалуйста, выберите +, -, * или /';

    }
}